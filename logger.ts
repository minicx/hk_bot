import winston = require("winston");
const logger = winston.createLogger({
    level: "debug",
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.printf((info) => {
            return `${new Date().toLocaleTimeString()} | ${info.level} - ${
                info.message
            } `;
        })
    ),
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({ filename: "errors.log", level: "error" }),
        new winston.transports.File({ filename: "info.log" }),
    ],
});

export default logger;
