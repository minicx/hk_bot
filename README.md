<div align="center">
    <h1><b>HK_BOT🤖</b></h1>
	<a href="https://hits.sh/git.disroot.org/minicx/hk_bot/"><img alt="Hits" src="https://hits.sh/git.disroot.org/minicx/hk_bot.svg"/></a>
	<a href="https://git.disroot.org/minicx/hk_bot/LiCENSE"><img src="https://shields.io/badge/license-MIT-green" alt='License'/></a>
</div>

# Installing

1. #### Cloning repo

   To do this run `git clone https://git.disroot.org/minicx/hk_bot.git`
2. #### Installing all dependencies

   Run in `hk_bot` directory `npm install` or `yarn install`
3. #### Starting

   Run `npm start` or `yarn start`

   <details>
      <summary>Other commands</summary>

   | Command | Description                                                        |
   | ------- | ------------------------------------------------------------------ |
   | build   | just compile all `.ts` files to `build` directory into `.js` |

   </details>

# Format of [Faucet](https://faucetpay.io) wallets data

* **❗ The script currently does not use the email or password of the faucet account ❗**
* **JSON file must be in root of `hk_bot` directory**

* <details>
   <summary><b>Explanation.json</b></summary>

     ```json
    {
      "<EMAIL>": {
        "mail_passphrase": "<PASSWORD OF MAIL>",
        "faucetpay": {
          "username": "<USERNAME OF FAUCET ACCOUNT>",
          "password": "<PASSWORD OF FAUCET ACCOUNT>"
        }
      }
    }
    ```

     </details>


# Creator and contributors
<div align='center'> 
    <img src="https://git.disroot.org/avatars/cf8b3507d9ab93a478052ffd7e750f12" width="100px" style="display:flex;border-radius: 75px;" alt="minicx_pict" >
    <a href="https://git.disroot.org/minicx"><b>Main devoloper</b></a>
</div>

  
