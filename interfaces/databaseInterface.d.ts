import { Fingerprint } from "fingerprint-generator";
export interface faucetMailInterface {
    faucet: {
        username: string;
        password: string;
    };
    mail: {
        address: string;
        password: string;
    };
}
export interface AccountCompletedGroupsTasksInterface {
    timeToLeave: number;
    groupID: string;
}

export interface AccountWithdrawInterface {}
export interface AccountInterface {
    phoneNumber: string;
    telegramID: number;
    apiID: number;
    apiHash: string;
    password: string;
    faucetMail: faucetMailInterface;
    stringSession: string;
    balance: number;
    withdraws: AccountWithdrawInterface[];
    completedGroupsTasks: AccountCompletedGroupsTasksInterface[];
    canBeRefferal: boolean;
    browserFingerprint: Fingerprint;
    refferal: number | null;
}
export interface DatabaseInterface {
    accounts: AccountInterface[];
}
