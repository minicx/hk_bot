import * as telegram from "telegram";
import { AccountInterface } from "./databaseInterface";

export interface prevTaskInterface {
    task: telegram.Api.Message;
    lastMessage: telegram.Api.Message;
}

export interface portsInterface {
    http: number;
    socks: number;
    control: number;
    minerPort?: number;
}

export interface usedIPInterface {
    worker: AccountInterface;
    usedIps: string[];
    current: string;
}
