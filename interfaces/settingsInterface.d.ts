export interface settingsInterface {
    logLevel: "debug" | "info" | "error";
    pararels: number;
    mainCrypto: "TRX";
    minimalToWithdraw: number;
    maxRefferals: number;
    // to find
    botMessages: {
        verification: string;
        tasksSelector: string;
        taskOver: string;
        taskComplete: string;
        notInGroup: string;
        oldMessage: string;
    };
    // to send
    botButtons: {
        earn: string;
        balance: string;
        withdraw: string;
    };
    // to join
    telegramLinks: {
        botLink: string;
        groupsToJoin: string[];
    };
    sleepTime: {
        afterDoing: number[];
        betweenSessions: number[];
    };
    shuffleAccounts: boolean;
    bypassMode: boolean;
}
